from audioop import reverse

from django.shortcuts import get_object_or_404, render

from .aemetparser import AemetChannel
from .wikipediaparser import  WikipediaChannel

from .models import Youtube, Comentario, Vote_Youtube, Aemet, ComentarioAemet, Vote_Aemet, Aemet_Cover
from .models import ComentarioWikipedia, Vote_Wikipedia, Wikipedia, All_Content, Mode_User
from django.http import HttpResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from django.contrib.auth import logout
from django.shortcuts import redirect
from django.utils import timezone
from django.contrib.auth.models import User
from django import forms
from django.contrib.auth import authenticate, login

import json
import urllib.request

@csrf_exempt
def index(request):
    msg = "hola"

    if request.method == "POST":
        # Coger el valor que haya en el cuerpo de HTTP
        action = request.POST["action"]

        if action == "Create user":
            username = request.POST["username"]
            password = request.POST["password"]
            email = request.POST["email"]

            if User.DoesNotExist:
                if username and password and email:
                    user = User.objects.create_user(username, email, password)
                    user.save()
            else:
                msg = "El usario ya está registrado"

        elif action == "login":
            username = request.POST["username"]
            password = request.POST["password"]
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
            else:
                msg = "Usuario o Contraseña erróneas"

        elif action == "Enviar":
            url = request.POST["URL"]
            if url.find('https://www.youtube.com') >= 0:
                description = request.POST["Description"]
                title_user = request.POST["title_user"]
                id_video = url.split('=')[-1]
                url_jason ="https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + str(id_video)


                video_embed = "https://www.youtube.com/embed/" + str(id_video)


                with urllib.request.urlopen(url_jason) as json_doc:
                    json_str = json_doc.read().decode(encoding="ISO-8859-1")
                    munis = json.loads(json_str)
                    c = Youtube(title=munis['title'], author_name=munis['author_name'], author_url=munis['author_url'],
                                video_embed=video_embed, youtube_url=url, description=description, title_user=title_user,
                                user_post=request.user.username)
                    c.save()

                    d = All_Content(title_user=title_user, description=description, url=url, user_post=request.user.username)
                    d.save()

            elif url.find('http://www.aemet.es') >= 0:
                description = request.POST["Description"]
                title_user = request.POST["title_user"]
                corte = url.split('/')[-1]
                num = corte.split('-id')[1]
                city = corte.split('-id')[0]
                msg = num + city
                url_xml = "https://www.aemet.es/xml/municipios/localidad_" + num + ".xml"
                xmlStream = urllib.request.urlopen(url_xml)
                aemet = AemetChannel(xmlStream)
                datos_aemet = aemet.prediccion()

                d = Aemet_Cover(copyright=datos_aemet[1]['copyright'], title_user=title_user, description=description,
                                city=city, province=datos_aemet[1]['province'], url_page=url,user_post=request.user.username)
                d.save()
                contenido = get_object_or_404(Aemet_Cover, title_user=title_user)
                for datos in datos_aemet:
                    c = Aemet(contenido=contenido, day=datos['day'], temp_max=datos['temp_max'], temp_min=datos['temp_min'],
                            sens_max=datos['sens_max'], sens_min=datos['sens_min'], hum_max=datos['hum_max'],
                            hum_min=datos['hum_min'])
                    c.save()

                d = All_Content(title_user=title_user, description=description, url=url,
                                user_post=request.user.username)
                d.save()
            elif url.find('https://es.wikipedia.org') >= 0:
                description = request.POST["Description"]
                title_user = request.POST["title_user"]
                articulo = url.split('/')[-1]
                url_articulo = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ \
                               articulo + '&prop=extracts&exintro&explaintext'

                xmlStream = urllib.request.urlopen(url_articulo)
                wikipedia_articulo = WikipediaChannel(xmlStream)
                articulo_text = wikipedia_articulo.articulo()

                url_foto = 'https://es.wikipedia.org/w/api.php?action=query&titles='+ \
                           articulo + '&prop=pageimages&format=json&pithumbsize=100'

                with urllib.request.urlopen(url_foto) as json_doc:
                    json_str = json_doc.read().decode(encoding="ISO-8859-1")
                    munis = json.dumps(json.loads(json_str))
                    try:
                        articulo_photo = munis.split('"source"')[1].split('"width"')[0][3:][:-3]
                    except:
                        articulo_photo = ""
                    c = Wikipedia(title_user=title_user, description=description, articulo=articulo,
                                  articulo_text=articulo_text, url_page=url, articulo_photo=articulo_photo,
                                  user_post=request.user.username)
                    c.save()
                    d = All_Content(title_user=title_user, description=description, url=url,
                                    user_post=request.user.username)
                    d.save()


        elif action == "Me gusta":
            llave = request.POST["name_post"]
            try:
                youtube = Youtube.objects.get(title_user=llave)

                try:
                    content_vote = Vote_Youtube.objects.get(vote_user=request.user.username, content=youtube)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        youtube.vote_positive = youtube.vote_positive + 1
                        youtube.vote_negative = youtube.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = youtube.vote_negative
                        all_content.vote_positive = youtube.vote_positive

                        youtube.save()
                        all_content.save()
                except Vote_Youtube.DoesNotExist:
                    q = Vote_Youtube(content=youtube, vote_user=request.user.username, vote="Positive")
                    q.save()
                    youtube.vote_positive = youtube.vote_positive + 1
                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = youtube.vote_positive

                    youtube.save()
                    all_content.save()


            except Youtube.DoesNotExist:
                s = 0
            # No hacemos nada
            try:

                aemet = Aemet_Cover.objects.get(title_user=llave)

                try:
                    content_vote = Vote_Aemet.objects.get(vote_user=request.user.username, content=aemet)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        aemet.vote_positive = aemet.vote_positive + 1
                        aemet.vote_negative = aemet.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = aemet.vote_negative
                        all_content.vote_positive = aemet.vote_positive

                        all_content.save()
                        aemet.save()
                except Vote_Aemet.DoesNotExist:
                    q = Vote_Aemet(content=aemet, vote_user=request.user.username, vote="Positive")
                    q.save()
                    aemet.vote_positive = aemet.vote_positive + 1
                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = aemet.vote_positive

                    aemet.save()
                    all_content.save()
            except Aemet_Cover.DoesNotExist:
                s = 0
            # No hacemos nada
            try:

                wikipedia = Wikipedia.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Wikipedia.objects.get(vote_user=request.user.username, content=wikipedia)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        wikipedia.vote_positive = wikipedia.vote_positive + 1
                        wikipedia.vote_negative = wikipedia.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = wikipedia.vote_negative
                        all_content.vote_positive = wikipedia.vote_positive

                        wikipedia.save()
                        all_content.save()
                except Vote_Wikipedia.DoesNotExist:
                    q = Vote_Wikipedia(content=wikipedia, vote_user=request.user.username, vote="Positive")
                    q.save()
                    wikipedia.vote_positive = wikipedia.vote_positive + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = wikipedia.vote_positive

                    wikipedia.save()
                    all_content.save()
            except Wikipedia.DoesNotExist:
                s = 0
                # No hacemos nada

        elif action == "No me gusta":
            llave = request.POST["name_post"]
            try:

                youtube = Youtube.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Youtube.objects.get(vote_user=request.user.username, content=youtube)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        youtube.vote_positive = youtube.vote_positive - 1
                        youtube.vote_negative = youtube.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = youtube.vote_negative
                        all_content.vote_positive = youtube.vote_positive

                        youtube.save()
                        all_content.save()
                except Vote_Youtube.DoesNotExist:
                    q = Vote_Youtube(content=youtube, vote_user=request.user.username, vote="Negative")
                    q.save()
                    youtube.vote_negative = youtube.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = youtube.vote_negative

                    youtube.save()
                    all_content.save()
            except Youtube.DoesNotExist:
                s = 0
            # No hacemos nada
            try:

                aemet = Aemet_Cover.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Aemet.objects.get(vote_user=request.user.username, content=aemet)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        aemet.vote_positive = aemet.vote_positive - 1
                        aemet.vote_negative = aemet.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = aemet.vote_negative
                        all_content.vote_positive = aemet.vote_positive

                        aemet.save()
                        all_content.save()
                except Vote_Aemet.DoesNotExist:
                    q = Vote_Aemet(content=aemet, vote_user=request.user.username, vote="Negative")
                    q.save()
                    aemet.vote_negative = aemet.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = aemet.vote_negative

                    aemet.save()
                    all_content.save()
            except Aemet_Cover.DoesNotExist:
                s = 0
                # No hacemos nada
            try:

                wikipedia = Wikipedia.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Wikipedia.objects.get(vote_user=request.user.username, content=wikipedia)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        wikipedia.vote_positive = wikipedia.vote_positive - 1
                        wikipedia.vote_negative = wikipedia.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = wikipedia.vote_negative
                        all_content.vote_positive = wikipedia.vote_positive

                        wikipedia.save()
                        all_content.save()
                except Vote_Wikipedia.DoesNotExist:
                    q = Vote_Wikipedia(content=wikipedia, vote_user=request.user.username, vote="Negative")
                    q.save()
                    wikipedia.vote_negative = wikipedia.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = wikipedia.vote_negative

                    wikipedia.save()
                    all_content.save()
            except Wikipedia.DoesNotExist:
                s = 0
                # No hacemos nada

        elif action == "Dark":
            user = request.POST["user"]
            try:
                content = Mode_User.objects.get(user=user)
                if content.mode == "Light":
                    content.mode = "Dark"
                    content.save()
            except Mode_User.DoesNotExist:
                mode_usuario = Mode_User(user=user, mode='Dark')
                mode_usuario.save()

        elif action == "Light":
            user = request.POST["user"]
            try:
                content = Mode_User.objects.get(user=user)
                if content.mode == "Dark":
                    content.mode = "Light"
                    content.save()
            except Mode_User.DoesNotExist:
                mode_user = Mode_User(user=user, mode='Ligth')
                mode_user.save()

    user_authenticated = request.user.is_authenticated
    content_youtube = Youtube.objects.all()
    content_aemet = Aemet_Cover.objects.all()
    content_wikipedia = Wikipedia.objects.all()
    all_content = All_Content.objects.all()
    len_all_content = len(all_content)
    if len_all_content > 10:
        last_all_content = All_Content.objects.all()[len_all_content - 10:len_all_content]
        last10_all_content = reversed(last_all_content)
    else:
        last10_all_content = reversed(all_content)

    mode = ""
    if request.user.is_authenticated:
        try:
            mode_user = Mode_User.objects.get(user=request.user.username)
            mode = mode_user.mode
        except Mode_User.DoesNotExist:
            s = 0

    context = {'user_authenticated': user_authenticated,
               'content_youtube': content_youtube,
               'user_on': request.user.username,
               'content_aemet': content_aemet,
               'content_wikipedia': content_wikipedia,
               'last10_all_content': last10_all_content,
               'mode': mode,}
    # return HttpResponse (template.render(context,request))
    return render(request, "LoVisto/index.html", context)
def logout_view(request):
    logout(request)
    return redirect("/LoVisto")

def get_content(request, llave):
    if request.method == "POST":
        action = request.POST["action"]
        if action == "Enviar Comentario":
            try:

                contenido = Youtube.objects.get(title_user=llave)
                commentary = request.POST["Commentary"]
                fecha = timezone.now()
                if commentary != "":
                    q = Comentario(contenido=contenido, comentario=commentary, fecha=fecha, user_comentario=request.user.username)
                    q.save()
                    contenido.num_comentario = contenido.num_comentario + 1
                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.num_comentario = contenido.num_comentario
                    all_content.save()
                    contenido.save()
            except Youtube.DoesNotExist:
                s = 0
                # No hacemos nada
            try:
                contenido = Aemet_Cover.objects.get(title_user=llave)
                commentary = request.POST["Commentary"]
                fecha = timezone.now()
                if commentary != "":
                    q = ComentarioAemet(contenido=contenido, comentario=commentary, fecha=fecha,
                                   user_comentario=request.user.username)
                    q.save()
                    contenido.num_comentario = contenido.num_comentario + 1
                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.num_comentario = contenido.num_comentario
                    all_content.save()
                    contenido.save()
            except Aemet_Cover.DoesNotExist:
                s = 0
                # No hacemos nada
            try:
                contenido = Wikipedia.objects.get(title_user=llave)
                commentary = request.POST["Commentary"]
                fecha = timezone.now()
                if commentary != "":
                    q = ComentarioWikipedia(contenido=contenido, comentario=commentary, fecha=fecha,
                                   user_comentario=request.user.username)
                    q.save()
                    contenido.num_comentario = contenido.num_comentario + 1
                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.num_comentario = contenido.num_comentario

                    all_content.save()
                    contenido.save()
            except Wikipedia.DoesNotExist:
                s = 0
                # No hacemos nada
        elif action == "Me gusta":
            try:
                youtube = Youtube.objects.get(title_user=llave)

                try:
                    content_vote = Vote_Youtube.objects.get(vote_user=request.user.username, content=youtube)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        youtube.vote_positive = youtube.vote_positive + 1
                        youtube.vote_negative = youtube.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = youtube.vote_negative
                        all_content.vote_positive = youtube.vote_positive

                        youtube.save()
                        all_content.save()

                except Vote_Youtube.DoesNotExist:
                    q = Vote_Youtube(content=youtube, vote_user=request.user.username, vote="Positive")
                    q.save()
                    youtube.vote_positive = youtube.vote_positive + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = youtube.vote_positive

                    youtube.save()
                    all_content.save()
            except Youtube.DoesNotExist:
                s = 0
                # No hacemos nada
            try:

                aemet = Aemet_Cover.objects.get(title_user=llave)

                try:
                    content_vote = Vote_Aemet.objects.get(vote_user=request.user.username, content=aemet)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        aemet.vote_positive = aemet.vote_positive + 1
                        aemet.vote_negative = aemet.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = aemet.vote_negative
                        all_content.vote_positive = aemet.vote_positive

                        aemet.save()
                        all_content.save()
                except Vote_Aemet.DoesNotExist:
                    q = Vote_Aemet(content=aemet, vote_user=request.user.username, vote="Positive")
                    q.save()
                    aemet.vote_positive = aemet.vote_positive + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = aemet.vote_positive

                    aemet.save()
            except Aemet_Cover.DoesNotExist:
                s = 0
                # No hacemos nada
            try:

                wikipedia = Wikipedia.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Wikipedia.objects.get(vote_user=request.user.username, content=wikipedia)
                    if content_vote.vote == "Negative":
                        content_vote.vote = "Positive"
                        content_vote.save()
                        wikipedia.vote_positive = wikipedia.vote_positive + 1
                        wikipedia.vote_negative = wikipedia.vote_negative - 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = wikipedia.vote_negative
                        all_content.vote_positive = wikipedia.vote_positive

                        wikipedia.save()
                        all_content.save()
                except Vote_Wikipedia.DoesNotExist:
                    q = Vote_Wikipedia(content=wikipedia, vote_user=request.user.username, vote="Positive")
                    q.save()
                    wikipedia.vote_positive = wikipedia.vote_positive + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_positive = wikipedia.vote_positive

                    wikipedia.save()
                    all_content.save()
            except Wikipedia.DoesNotExist:
                s = 0
                # No hacemos nada

        elif action == "No me gusta":
            try:

                youtube = Youtube.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Youtube.objects.get(vote_user=request.user.username, content=youtube)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        youtube.vote_positive = youtube.vote_positive - 1
                        youtube.vote_negative = youtube.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = youtube.vote_negative
                        all_content.vote_positive = youtube.vote_positive

                        youtube.save()
                        all_content.save()
                except Vote_Youtube.DoesNotExist:
                    q = Vote_Youtube(content=youtube, vote_user=request.user.username, vote="Negative")
                    q.save()
                    youtube.vote_negative = youtube.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = youtube.vote_negative

                    youtube.save()
                    all_content.save()
            except Youtube.DoesNotExist:
                s = 0
                # No hacemos nada
            try:

                aemet = Aemet_Cover.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Aemet.objects.get(vote_user=request.user.username, content=aemet)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        aemet.vote_positive = aemet.vote_positive - 1
                        aemet.vote_negative = aemet.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = aemet.vote_negative
                        all_content.vote_positive = aemet.vote_positive

                        aemet.save()
                        all_content.save()
                except Vote_Aemet.DoesNotExist:
                    q = Vote_Aemet(content=aemet, vote_user=request.user.username, vote="Negative")
                    q.save()
                    aemet.vote_negative = aemet.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = aemet.vote_negative

                    aemet.save()
                    all_content.save()
            except Aemet_Cover.DoesNotExist:
                s = 0
                # No hacemos nada
            try:

                wikipedia = Wikipedia.objects.get(title_user=llave)
                try:
                    content_vote = Vote_Wikipedia.objects.get(vote_user=request.user.username, content=wikipedia)
                    if content_vote.vote == "Positive":
                        content_vote.vote = "Negative"
                        content_vote.save()
                        wikipedia.vote_positive = wikipedia.vote_positive - 1
                        wikipedia.vote_negative = wikipedia.vote_negative + 1

                        all_content = All_Content.objects.get(title_user=llave)
                        all_content.vote_negative = wikipedia.vote_negative
                        all_content.vote_positive = wikipedia.vote_positive

                        wikipedia.save()
                        all_content.save()
                except Vote_Wikipedia.DoesNotExist:
                    q = Vote_Wikipedia(content=wikipedia, vote_user=request.user.username, vote="Negative")
                    q.save()
                    wikipedia.vote_negative = wikipedia.vote_negative + 1

                    all_content = All_Content.objects.get(title_user=llave)
                    all_content.vote_negative = wikipedia.vote_negative

                    wikipedia.save()
                    all_content.save()
            except Wikipedia.DoesNotExist:
                s = 0
                # No hacemos nada

        elif action == "Dark":
            user = request.POST["user"]
            try:
                content = Mode_User.objects.get(user=user)
                if content.mode == "Light":
                    content.mode = "Dark"
                    content.save()
            except Mode_User.DoesNotExist:
                mode_usuario = Mode_User(user=user, mode='Dark')
                mode_usuario.save()

        elif action == "Light":
            user = request.POST["user"]
            try:
                content = Mode_User.objects.get(user=user)
                if content.mode == "Dark":
                    content.mode = "Light"
                    content.save()
            except Mode_User.DoesNotExist:
                mode_user = Mode_User(user=user, mode='Ligth')
                mode_user.save()

    mode = ""
    if request.user.is_authenticated:
        try:
            mode_user = Mode_User.objects.get(user=request.user.username)
            mode = mode_user.mode
        except Mode_User.DoesNotExist:
            s = 0

    try:

        content = Youtube.objects.get(title_user=llave)
        user_authenticated = request.user.is_authenticated
        context = {'content': content,
                'user_authenticated': user_authenticated,
                   'mode': mode,
                   'user_on': request.user.username,}
        return render(request, 'LoVisto/content_youtube.html', context)
    except Youtube.DoesNotExist:
        s = 0
            #No hacemos nada
    try:
        content = Aemet_Cover.objects.get(title_user=llave)
        user_authenticated = request.user.is_authenticated
        context = {'content': content,
            'user_authenticated': user_authenticated,
                   'mode': mode,
                   'user_on': request.user.username,}
        return render(request, 'LoVisto/content_aemet.html', context)
    except Aemet_Cover.DoesNotExist:
        s = 0
    try:
        content = Wikipedia.objects.get(title_user=llave)
        user_authenticated = request.user.is_authenticated
        context = {'content': content,
                    'user_authenticated': user_authenticated,
                   'mode': mode,
                   'user_on': request.user.username,}
        return render(request, 'LoVisto/content_wikipedia.html', context)
    except Wikipedia.DoesNotExist:
        s = 0
        # No hacemos nada


    context = {'llave': llave,}
    return render(request, 'LoVisto/error.html', context)

def index_xml(request):
    content_youtube = Youtube.objects.all()
    content_aemet = Aemet_Cover.objects.all()
    content_wikipedia = Wikipedia.objects.all()
    context = {'content_aemet': content_aemet,
               'content_youtube': content_youtube,
               'content_wikipedia': content_wikipedia, }

    return render(request, "xml/main.xml", context, content_type="text/xml")

def index_json(request):
    content_youtube = Youtube.objects.all()
    content_aemet = Aemet_Cover.objects.all()
    content_wikipedia = Wikipedia.objects.all()
    context = {'content_aemet': content_aemet,
               'content_youtube': content_youtube,
               'content_wikipedia': content_wikipedia, }

    return render(request, "json/main.json", context,  content_type="text/json")

def get_content_xml(request, llave):
    try:

        content = Youtube.objects.get(title_user=llave)
        context = {'content': content,
                }
        return render(request, 'xml/content_youtube.xml', context, content_type="text/xml")
    except Youtube.DoesNotExist:
        s = 0
            #No hacemos nada
    try:
        content = Aemet_Cover.objects.get(title_user=llave)
        context = {'content': content,}
        return render(request, 'xml/content_aemet.xml', context, content_type="text/xml")
    except Aemet_Cover.DoesNotExist:
        s = 0
    try:
        content = Wikipedia.objects.get(title_user=llave)
        context = {'content': content, }
        return render(request, 'xml/content_wikipedia.xml', context, content_type="text/xml")
    except Aemet_Cover.DoesNotExist:
        s = 0
        # No hacemos nada


    context = {'llave': llave,}
    return render(request, 'LoVisto/error.html', context)

def get_content_json(request, llave):
    try:

        content = Youtube.objects.get(title_user=llave)
        context = {'content': content,
                }
        return render(request, "json/content_youtube.json", context, content_type="text/json")
    except Youtube.DoesNotExist:
        s = 0
            #No hacemos nada
    try:
        content = Aemet_Cover.objects.get(title_user=llave)
        context = {'content': content,}
        return render(request, "json/content_aemet.json", context, content_type="text/json")
    except Aemet_Cover.DoesNotExist:
        s = 0
    try:
        content = Wikipedia.objects.get(title_user=llave)
        context = {'content': content, }
        return render(request, "json/content_wikipedia.json", context, content_type="text/json")
    except Aemet_Cover.DoesNotExist:
        s = 0
        # No hacemos nada


    context = {'llave': llave,}
    return render(request, 'LoVisto/error.html', context)