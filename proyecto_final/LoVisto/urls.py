from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('format=xml', views.index_xml),
    path('format=json', views.index_json),
    path('<str:llave>/format=xml', views.get_content_xml),
    path('<str:llave>/format=json', views.get_content_json),
    path('logout', views.logout_view),
    path('<str:llave>', views.get_content, name='get_content')
]