from django.db import models

class Youtube(models.Model):
    title = models.CharField(max_length=128)
    author_name = models.CharField(max_length=128)
    author_url = models.CharField(max_length=128)
    video_embed = models.CharField(max_length=128)
    youtube_url = models.CharField(max_length=128)
    title_user = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    fecha = models.DateTimeField(auto_now=True)
    vote_negative = models.IntegerField(default=0)
    vote_positive = models.IntegerField(default=0)
    user_post = models.CharField(max_length=64)
    num_comentario = models.IntegerField(default=0)
    def __str__(self):
        return self.title_user + " = " + str(self.id)

class Vote_Youtube(models.Model):
    content = models.ForeignKey(Youtube, on_delete=models.CASCADE)
    vote_user = models.CharField(max_length=128)
    vote = models.CharField(max_length=64)


class Comentario(models.Model):
    contenido = models.ForeignKey(Youtube, on_delete=models.CASCADE)
    comentario = models.TextField(blank=False)
    fecha = models.DateTimeField(auto_now=True)
    user_comentario = models.CharField(max_length=128)


class Aemet_Cover(models.Model):
    title_user = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    fecha = models.DateTimeField(auto_now=True)
    vote_negative = models.IntegerField(default=0)
    vote_positive = models.IntegerField(default=0)
    copyright = models.CharField(max_length=128)
    city = models.CharField(max_length=128)
    province = models.CharField(max_length=128)
    url_page = models.CharField(max_length=128)
    user_post = models.CharField(max_length=64)
    num_comentario = models.IntegerField(default=0)
    def __str__(self):
        return self.title_user + " = " + str(self.id)

class Aemet(models.Model):
    contenido = models.ForeignKey(Aemet_Cover, on_delete=models.CASCADE)
    day = models.CharField(max_length=64)
    temp_max = models.CharField(max_length=64)
    temp_min = models.CharField(max_length=64)
    sens_max = models.CharField(max_length=64)
    sens_min = models.CharField(max_length=64)
    hum_max = models.CharField(max_length=64)
    hum_min = models.CharField(max_length=64)


class ComentarioAemet(models.Model):
    contenido = models.ForeignKey(Aemet_Cover, on_delete=models.CASCADE)
    comentario = models.TextField(blank=False)
    fecha = models.DateTimeField(auto_now=True)
    user_comentario = models.CharField(max_length=128)

class Vote_Aemet(models.Model):
    content = models.ForeignKey(Aemet_Cover, on_delete=models.CASCADE)
    vote_user = models.CharField(max_length=128)
    vote = models.CharField(max_length=64)

class Wikipedia(models.Model):
    title_user = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    fecha = models.DateTimeField(auto_now=True)
    articulo = models.CharField(max_length=128)
    articulo_text = models.CharField(max_length=400)
    articulo_photo = models.CharField(max_length=128)
    url_page = models.CharField(max_length=128)
    vote_negative = models.IntegerField(default=0)
    vote_positive = models.IntegerField(default=0)
    user_post = models.CharField(max_length=64)
    num_comentario = models.IntegerField(default=0)

    def __str__(self):
        return self.title_user + " = " + str(self.id)

class ComentarioWikipedia(models.Model):
    contenido = models.ForeignKey(Wikipedia, on_delete=models.CASCADE)
    comentario = models.TextField(blank=False)
    fecha = models.DateTimeField(auto_now=True)
    user_comentario = models.CharField(max_length=128)

class Vote_Wikipedia(models.Model):
    content = models.ForeignKey(Wikipedia, on_delete=models.CASCADE)
    vote_user = models.CharField(max_length=128)
    vote = models.CharField(max_length=64)


class All_Content(models.Model):
    title_user = models.CharField(max_length=128)
    description = models.CharField(max_length=128)
    fecha = models.DateTimeField(auto_now=True)
    vote_negative = models.IntegerField(default=0)
    vote_positive = models.IntegerField(default=0)
    num_comentario = models.IntegerField(default=0)
    url = models.CharField(max_length=128)
    user_post = models.CharField(max_length=64)

class Mode_User(models.Model):
    user = models.CharField(max_length=128)
    mode = models.CharField(max_length=128)