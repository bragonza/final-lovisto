# Generated by Django 3.1.7 on 2021-06-05 18:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LoVisto', '0017_auto_20210605_1409'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mode_User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.CharField(max_length=128)),
                ('mode', models.CharField(max_length=128)),
            ],
        ),
    ]
