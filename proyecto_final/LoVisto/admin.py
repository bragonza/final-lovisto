from django.contrib import admin
from .models import Youtube, Aemet_Cover, Wikipedia, All_Content
admin.site.register(Youtube)
admin.site.register(Aemet_Cover)
admin.site.register(Wikipedia)
admin.site.register(All_Content)

