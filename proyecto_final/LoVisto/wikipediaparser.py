from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class WikipediaHandler(ContentHandler):

    def __init__ (self):
        self.inContent = False
        self.content = ""
        self.texto = ""
        self.articulo = ""

    def startElement (self, name, attrs):
        if name == 'extract':
            self.inContent = True


    def endElement (self, name):
        global articulo

        if name == 'extract':
            self.texto = self.content
            self.content = ""
            self.inContent = False
            self.articulo = self.texto

    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class WikipediaChannel:

    def __init__ (self, stream):
        self.parser = make_parser()
        self.handler = WikipediaHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def articulo (self):
        return self.handler.articulo