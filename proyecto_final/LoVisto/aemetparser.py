from xml.sax.handler import ContentHandler
from xml.sax import make_parser


class AemetHandler(ContentHandler):

    def __init__ (self):
        self.inContent = False
        self.content = ""
        self.inDia = False
        self.inTemperatura = False
        self.inSens = False
        self.inHumedad = False
        self.copy = ""
        self.provincia = ""
        self.dia = ""
        self.temp_max = ""
        self.temp_min = ""
        self.sens_max = ""
        self.sens_min = ""
        self.hum_max = ""
        self.hum_min = ""
        self.prediccion = []

    def startElement (self, name, attrs):
        if name == 'copyright':
            self.inContent = True
        elif name == 'nombre':
            self.inContent = True
        elif name == 'provincia':
            self.inContent = True
        elif name == 'dia':
            self.dia = attrs.get('fecha')
            self.inDia = True
        elif self.inDia:
            if name == 'temperatura':
                self.inTemperatura = True
            elif name == 'sens_termica':
                self.inSens = True
            elif name == 'humedad_relativa':
                self.inHumedad = True
            elif self.inTemperatura:
                if name == 'minima':
                    self.inContent = True
                elif name == 'maxima':
                    self.inContent = True

            elif self.inSens:
                if name == 'minima':
                    self.inContent = True
                elif name == 'maxima':
                    self.inContent = True

            elif self.inHumedad:
                if name == 'minima':
                    self.inContent = True
                elif name == 'maxima':
                    self.inContent = True



    def endElement (self, name):
        global prediccion

        if name == 'copyright':
            self.copy = self.content
            self.content = ""
            self.inContent = False
        elif name == 'nombre':
            self.ciudad = self.content
            self.content = ""
            self.inContent = False
        elif name == 'provincia':
            self.provincia = self.content
            self.content = ""
            self.inContent = False
        elif name == 'dia':
            self.inDia = False
            self.prediccion.append ({'copyright': self.copy,
                                     'city': self.ciudad,
                                     'province': self.provincia,
                                     'day': self.dia,
                                     'temp_max': self.temp_max,
                                     'temp_min': self.temp_min,
                                     'sens_max': self.sens_max,
                                     'sens_min': self.sens_min,
                                     'hum_max': self.hum_max,
                                     'hum_min': self.hum_min})
        elif self.inDia:
            if name == 'temperatura':
                self.inTemperatura = False
            elif name == 'sens_termica':
                self.inSens = False
            elif name == 'humedad_relativa':
                self.inHumedad = False
            elif self.inTemperatura:
                if name == 'minima':
                    self.temp_min = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'maxima':
                    self.temp_max = self.content
                    self.content = ""
                    self.inContent = False

            elif self.inSens:
                if name == 'minima':
                    self.sens_min = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'maxima':
                    self.sens_max = self.content
                    self.content = ""
                    self.inContent = False

            elif self.inHumedad:
                if name == 'minima':
                    self.hum_min = self.content
                    self.content = ""
                    self.inContent = False
                elif name == 'maxima':
                    self.hum_max = self.content
                    self.content = ""
                    self.inContent = False


    def characters (self, chars):
        if self.inContent:
            self.content = self.content + chars

class AemetChannel:

    def __init__ (self, stream):
        self.parser = make_parser()
        self.handler = AemetHandler()
        self.parser.setContentHandler(self.handler)
        self.parser.parse(stream)

    def prediccion (self):
        return self.handler.prediccion
