# Final Lo Visto

Práctica final del curso 2020/21

# Entrega practica#
# Datos
* Nombre:Brayan Antony Gonzales Carrera
* Titulación: Sistemas de Telecomunicaciones
* Despliegue (url):http://brayan.pythonanywhere.com/LoVisto/
* Video básico (url): https://www.youtube.com/watch?v=YQdCvG0ZAC8
* Video básico con música de fondo (url):https://www.youtube.com/watch?v=Uz9EsydezSc&t=78s
* Video parte opcional (url):https://www.youtube.com/watch?v=XaaRt_uqms8
## Cuenta Admin Site
*usuario/contrase~na:
admin-admin
## Cuentas usuarios
* usuario/contrase~na(repositorio git):
brayan-brayan
* En el caso del despliegue hemos añadido más cuentas:
antony-antony
sara-sara
## Resumen parte obligatoria
Mi recurso principal es el "LoVisto/" donde se mostrará la página principal. En esta página principal, en caso de no estar logeado, podrás iniciar sesión o crearte una cuenta. Una vez logeado podrás ingresar una url con un título y una descripción, tengo como url reconocibles las de wikipedia, la de aemet y la de youtube. Se podrá accder a las páginas de cada contenido para ello dispone de un link pinchable. Un usuario registrado puede votar y comentar(un voto por usuario y comentarios indefinidos). Támbien dispone el usuario de un modo oscuro.
## Lista partes opcionales
* Nombre parte: Servir todas las páginas en formato "xml" y "json"
para ello dispone en la cabecera un link pinchable para cargar estas páginas, o tambien se pude acceder de forma "/format=xml" o "/format=json",
una vez estando en las páginas podrá descargarse los documentos.
